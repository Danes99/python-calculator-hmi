# -*- coding: utf-8 -*-
"""
Created on Fri May 24 14:05:03 2019

@author: Clément

Projet IHM : Calculatrice scientifique + conversion de monnnaies
"""

from math import *

from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QAction, QApplication, QGridLayout, QMainWindow
from PyQt5.QtWidgets import QPushButton, QTextEdit, QStackedWidget, QWidget
from PyQt5.QtWidgets import QComboBox, QMessageBox


class Fenetre(QMainWindow):
    
    Mode_liste = ["Standard", "Scientifique", "Devise"]
    
    buttonMinSize = QSize(50, 50)
    buttonMaxSize = QSize(200, 200)

    def __init__(self):
        super().__init__()

        self.stack = QStackedWidget()
        
        # On definie les actions du menu
        self.menu = self.menuBar().addMenu("Mode")
        self.tabAction = []
        for i in range(3):
            self.tabAction +=  [QAction(Fenetre.Mode_liste[i], self)]
            self.tabAction[i].triggered.connect(self.switch)
            self.menu.addAction(self.tabAction[i])
        
        # TextBox de la calculatrice
        self.TextBox = QTextEdit("0")
        self.TextBox.setMaximumHeight(50)
        self.font = self.TextBox.font()
        self.font.setPointSize(self.TextBox.fontInfo().pixelSize() + 10)
        self.TextBox.setFont(self.font)
        
        # On initialise les 3 modes
        self.set_standard_window()
        self.set_scientifique_window()
        self.set_devise_window()
        
        
        # On initialise les 3 layouts
        self.gridTab = []
        for i in range(3):
            self.gridTab.append(QGridLayout())
            self.gridTab[i].setSpacing(0)
            page = QWidget()
            page.setLayout(self.gridTab[i])
            self.stack.addWidget(page)

        self.setCentralWidget(self.stack)

        self.switch_to_standard_mode()

        self.setMaximumSize(800, 600)
        self.setWindowTitle("Calculatrice")
        self.show()


    def set_standard_window(self):
        """
        Definie la page du mode standard de la calculatrice
        """
        
        # Composant qui regroupe tous les bouttons du mode standard
        self.buttonWidgetStandard = QWidget()
        self.gridButtonStandard = QGridLayout()
        self.gridButtonStandard.setSpacing(0)
        self.buttonWidgetStandard.setLayout(self.gridButtonStandard)
        
        # Tableau contenant tout les symboles pour le mode standard
        self.listSymbolsStandard = [
                "C", "←", "%", "/",
                "7", "8", "9", "*",
                "4", "5", "6", "-",
                "1", "2", "3", "+",
                ".", "0", '±', "="]
        
        # Tableau contenant toute les references des fonctions pour le mode standard
        self.listFunctionsStandard = [
                self.clear, self.delete_char, self.clicButton, self.clicButton,
                self.clicButton, self.clicButton, self.clicButton, self.clicButton,
                self.clicButton, self.clicButton, self.clicButton, self.clicButton,
                self.clicButton, self.clicButton, self.clicButton, self.clicButton,
                self.clicButton, self.clicButton, self.clicButton, self.calcul]
        
        # Initialisation de tout les boutons du mode standard
        for i in range(len(self.listSymbolsStandard)):
            button = QPushButton(self.listSymbolsStandard[i])
            button.setMinimumSize(Fenetre.buttonMinSize)
            button.setMaximumSize(Fenetre.buttonMaxSize)
            button.clicked.connect(self.listFunctionsStandard[i])
            self.gridButtonStandard.addWidget(button, i//4, i%4, 1, 1)            
        
        
    def set_scientifique_window(self):
        """
        Definie la page du mode scientifique de la calculatrice
        """
       
        # Composant qui regroupe tous les bouttons du mode scientifique
        self.buttonWidgetScientific = QWidget()
        self.gridButtonScienctific = QGridLayout()
        self.gridButtonScienctific.setSpacing(0)
        self.buttonWidgetScientific.setLayout(self.gridButtonScienctific)
        
        # Tableau contenant tout les symboles pour le mode scientifique
        self.listSymbolsScientific = [
                "sin", "cos", "tan", "%", "C", "←", "/",
                "arcsin", "arccos", "arctan", "7", "8", "9", "*",
                "sqrt", "ln", "log", "4", "5", "6", "-",
                "x²", "exp(x)", "10^x", "1", "2", "3", "+",
                "x^y", "(", ")", "!", "0", '.', "="]
        
        # Tableau contenant toute les references des fonctions pour le mode scientifique
        self.listFunctionsScientific = [
                self.put_sin, self.put_cos, self.put_tan, self.clicButton, self.clear, self.delete_char, self.clicButton,
                self.put_asin, self.put_acos, self.put_atan, self.clicButton, self.clicButton, self.clicButton, self.clicButton,
                self.put_sqrt, self.put_ln, self.put_log, self.clicButton, self.clicButton, self.clicButton, self.clicButton,
                self.put_square, self.put_exp, self.put_tenpow, self.clicButton, self.clicButton, self.clicButton, self.clicButton,
                self.put_xpowy, self.clicButton, self.clicButton, self.put_fact, self.clicButton, self.clicButton, self.calcul
                ]
        
        # Initialisation de tout les boutons du mode scientifique
        for i in range(len(self.listSymbolsScientific)):
            button = QPushButton(self.listSymbolsScientific[i])
            button.setMinimumSize(Fenetre.buttonMinSize)
            button.setMaximumSize(Fenetre.buttonMaxSize)
            button.clicked.connect(self.listFunctionsScientific[i])
            self.gridButtonScienctific.addWidget(button, i//7, i%7)
    
    
    def set_devise_window(self):
        """
        Definie la page du mode devise de la calculatrice, pour convertir les monnaies
        La monnaie de reference est le dollars US ($)
        """
        
        # Composant qui regroupe tous les bouttons du mode devise
        self.buttonWidgetDevise = QWidget()
        self.gridButtonDevise = QGridLayout()
        self.gridButtonDevise.setSpacing(0)
        self.buttonWidgetDevise.setLayout(self.gridButtonDevise)
        
        # TextBox de la monnaie convertie
        self.TextBoxMoney = QTextEdit("0")
        self.TextBoxMoney.setMaximumHeight(50)
        self.font = self.TextBoxMoney.font()
        self.font.setPointSize(self.TextBoxMoney.fontInfo().pixelSize() + 10)
        self.TextBoxMoney.setFont(self.font)
        self.gridButtonDevise.addWidget(self.TextBoxMoney, 0, 0, 1, 6)
        """
        Tableau contenant toute les monnaies pour le mode devise
        # ["Nom de la monnaie", float = valeur par rapport au dollars americain ($)]
        """
        self.listMoneyDevise =  [["Dollar Americain ($)", 1],
          ["Euro (€)", 0.89320],
          ["Livres Sterling (£)",0.78815],
          ["Dollar Canadian ($)", 1.34538],
          ["Yen (¥)", 109.49],
          ["Peso Mexicain ($)", 19.0365],
          ["Yuan (¥)", 6.90305],
          ["Rouble russe (₽)", 64.5685],
          ["Dollar Hong Kong ($)", 7.84850],
          ["Rupee Indian (INR)", 69.3850], 
          ["Dollar Australien ($)", 1.44362],
          ["Dollar Singapore ($)", 1.37536],
          
         ]
        
        # Combo Box
        self.comboBox1 = QComboBox()
        self.comboBox2 = QComboBox()
        
        for i in range(len(self.listMoneyDevise)):
            self.comboBox1.addItem(self.listMoneyDevise[i][0])
            self.comboBox2.addItem(self.listMoneyDevise[i][0])
        
        self.gridButtonDevise.addWidget(self.comboBox1, 1, 0)
        self.gridButtonDevise.addWidget(self.comboBox2, 1, 1)
        
        # Bouton Convertir les monnaies
        button = QPushButton("Convertir")
        button.clicked.connect(self.convertMoney)
        self.gridButtonDevise.addWidget(button, 2, 0, 1, 6)
        

    def switch(self):
        """
        Initialise le passage du mode actuelle vers un nouveau mode
        """
        event = self.sender()

        if event == self.tabAction[0]:
            self.stack.setCurrentIndex(0)
            self.switch_to_standard_mode()
            self.popup = QMessageBox(QMessageBox.Information, 'Message', 'Passage au mode standard.')
            self.popup.show()

        elif event == self.tabAction[1]:
            self.stack.setCurrentIndex(1)
            self.switch_to_scientifique_mode()
            self.popup = QMessageBox(QMessageBox.Information, 'Message', 'Passage au mode scientifique.')
            self.popup.show()

        elif event == self.tabAction[2]:
            self.stack.setCurrentIndex(2)
            self.switch_to_devise_mode()
            self.popup = QMessageBox(QMessageBox.Information, 'Message', 'Passage au mode devise.')
            self.popup.show()
        
    def switch_to_standard_mode(self):
        """
        Passage au mode standard de la calculatrice
        """
        self.gridTab[0].addWidget(self.TextBox, 0, 0)
        self.gridTab[0].addWidget(self.buttonWidgetStandard, 1, 0)

    def switch_to_scientifique_mode(self):
        """
        Passage au mode scientifique de la calculatrice
        """
        self.gridTab[1].addWidget(self.TextBox, 0, 0)
        self.gridTab[1].addWidget(self.buttonWidgetScientific, 1, 0)

    def switch_to_devise_mode(self):
        """
        Passage au mode devise de la calculatrice
        """
        self.gridTab[2].addWidget(self.TextBox, 0, 0)
        self.gridTab[2].addWidget(self.buttonWidgetDevise, 1, 0)
    
    def clicButton(self):
        text = self.sender().text()
        TextBox = self.TextBox.toPlainText()
        if text in "+-±%*/" and TextBox == "0":
            return
        elif TextBox == "0":
            self.TextBox.setText(text)
        elif TextBox[-1] in "+-*/." and text in "+*/.":
            return
        elif text == "±":
            try:
                self.TextBox.setText(str(-float(TextBox)))
                return
            except:
                self.errorMessage()
                return
        else:
            self.TextBox.setText(TextBox + text)
    
    
    def clear(self):
        self.TextBox.setText("0")

    def delete_char(self):
        text = self.TextBox.toPlainText()
        if len(text) == 1:
            self.TextBox.setText("0")
        else:
            self.TextBox.setText(text[0:-1])

    def calcul(self):
        
        TextBox = self.TextBox.toPlainText()
        
        try:
            exec("self.result = " + TextBox)
            self.TextBox.setText(str(self.result))
        except:
            self.errorMessage()
    
    def errorMessage(self):
        self.popup = QMessageBox(QMessageBox.Information, 'Error', 'Erreur !\nContenue non valide.\nVeuillez changer le contenue.')
        self.popup.show()
        
    
    """
    Les 14 prochaines fonctions sont utilisees dans le mode scientifique:
    put_sin, put_cos, put_tan, put_asin, put_acos, put_atan, put_sqrt, put_ln,
    put_log, put_square, put_exp, put_tenpow, put_xpowy, put_fact
    """
    
    def put_sin(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = sin(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_cos(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = cos(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_tan(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = tan(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_asin(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = asin(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_acos(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = acos(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_atan(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = atan(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_sqrt(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = sqrt(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_ln(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = log(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_log(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = log10(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_square(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = float(TextBox)**2
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_exp(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = exp(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_tenpow(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = pow(10, float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()
    
    def put_xpowy(self):
        TextBox = self.TextBox.toPlainText()
        try:
             self.TextBox.setText(str(TextBox + "**"))
        except:
             self.errorMessage()

    def put_fact(self):
         TextBox = self.TextBox.toPlainText()
         try:
             var = factorial(float(TextBox))
             exec("self.result = " + str(var))
             self.TextBox.setText(str(self.result))
         except:
             self.errorMessage()()
    
    def convertMoney(self):
        money1 = self.listMoneyDevise[self.comboBox1.currentIndex()][1]
        money2 = self.listMoneyDevise[self.comboBox2.currentIndex()][1]
        
        try:
            self.convertedMoney = round(float(self.TextBox.toPlainText())/money1*money2, 3)
            self.TextBoxMoney.setText(str(self.convertedMoney))
        except:
            self.errorMessage()



app = QApplication.instance()
if app is None:
    app = QApplication([])

calc = Fenetre()
app.exec()

